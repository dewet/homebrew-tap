require "language/go"

class HomeAssistantOsxPowerNotify < Formula
  desc "This is a small utility that hooks into IOKit's power management to receive notifications for power and display events. This gets translated into three states that can be reported via either MQTT or the Home Assistant REST API."
  version "v2.0.0"
  homepage "https://gitlab.com/dewet/home-assistant-osx-power-notify"
  url "https://gitlab.com/dewet/home-assistant-osx-power-notify/-/archive/#{version}/home-assistant-osx-power-notify-#{version}.tar.bz2"
  sha256 "cb5d89f70ac418c90dfb3c7e648f58acd55719c6c443ca8a91a066abd08e7a22"
  revision 1
  head "https://gitlab.com/dewet/home-assistant-osx-power-notify.git"
  depends_on "go" => :build

  def install
    ENV["GOPATH"] = buildpath
    ENV["GO111MODULE"] = "on"
    path = buildpath/"src/gitlab.com/dewet/home-assistant-osx-power-notify"
    path.install Dir["*"]
    cd path do
      system "go", "build", "-o", "#{bin}/powernotify"
    end
  end

  plist_options manual: "powernotify -config ~/.powernotify.yaml"

  def plist
    <<~EOS
      <?xml version="1.0" encoding="UTF-8"?>
      <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
      <plist version="1.0">
      <dict>
        <key>Label</key>
        <string>#{plist_name}</string>
        <key>ProgramArguments</key>
        <array>
            <string>#{bin}/powernotify</string>
            <string>-config</string>
            <string>#{etc}/powernotify.yaml</string>
        </array>
        <key>RunAtLoad</key>
        <true/>
        <key>KeepAlive</key>
        <true/>
      </dict>
      </plist>
    EOS
  end

  test do
    assert_match version.to_s, shell_output("#{bin}/powernotify", "-version")
  end

end 
